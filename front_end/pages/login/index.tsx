import { gql, useMutation, useQuery } from '@apollo/client';
import * as React from 'react';
import Home from '..';
import { LOG_IN } from '../graphql-client/queries';
import styles from './login.module.css';

export default function LoginPage() {
	const [username, setUsername] = React.useState('');
	const [password, setPassword] = React.useState('');
	const [logIn] = useMutation(LOG_IN, {
		onError: (err) => {
			alert(err.message)
		},
		onCompleted: (data) => {
			console.log(data)
			alert('Login successfully')
		}
	});

	const handleLogin = (e: React.FormEvent) => {
		e.preventDefault();
		logIn({
			variables: {
				input: {
					username,
					password
				}
			}
		})

	};

	return (
		<section className={styles.container}>
			<Home />
			<div className={styles.login_container}>
				<form className={styles.swapper_login} onSubmit={handleLogin}>
					<h2 className={styles.login_title}> LOGIN </h2>
					<div className={styles.login_username}>
						<p>Use Name: </p>
						<input
							type="text"
							placeholder="Enter your username"
							onChange={(e) => setUsername(e.target.value)}
						/>
					</div>
					<div className={styles.login_password}>
						<p>Password: </p>
						<input
							type="password"
							placeholder="Enter your password"
							onChange={(e) => setPassword(e.target.value)}
						/>
					</div>
					<button className={styles.login_submit} type="submit">
						Continue
					</button>
				</form>
			</div>
		</section>
	);
}
