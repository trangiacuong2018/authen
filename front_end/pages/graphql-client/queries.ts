import { gql } from '@apollo/client';

const CREATE_USER = gql`
	mutation CreateUser($input: UserInput!) {
		createUser(input: $input) {
			username
			password
			email
			phone
		}
	}
`;

const LOG_IN = gql`
	mutation LogIn($input: InputLogin!) {
		logIn(input: $input) {
			access_token
			user {
				username
			}
		}
	}
`;

const UPDATE_USER = gql`
	mutation updateUser {
		updateUser(input: $input) {
			username
			email
			phone
		}
	}
`;
export { CREATE_USER, LOG_IN,UPDATE_USER };
