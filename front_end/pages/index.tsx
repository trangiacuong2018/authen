import Link from 'next/link';
import styles from '../styles/Home.module.css';



const Home = () => {
	return (
			<section className={styles.container}>
				<header className={styles.navContainer}>
					<div className={styles.navContent}>
						<Link href="/" passHref>
							<button className={styles.aboutUsBtn}>
								<a>About Us</a>
							</button>
						</Link>
						<Link href="/login" passHref>
							<button className={styles.loginBtn}>
								<a>Login</a>
							</button>
						</Link>
						<Link href="/register" passHref>
							<button className={styles.registerBtn}>
								<a>Register</a>
							</button>
						</Link>
					</div>
				</header>
			</section>
		//  </Provider>
	);
};
export default Home;
