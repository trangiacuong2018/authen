
import { useMutation } from '@apollo/client'
import * as React from 'react'
import Home from '..'
import { CREATE_USER } from '../graphql-client/queries'
import styles from './register.module.css'


function RegisterPage() {
	const [username, setUsername] = React.useState<String>('')
	const [password, setPassword] = React.useState<String>('')
	const [phone, setPhone] = React.useState<String>('')
	const [email, setEmail] = React.useState<String>('')

	const [createUser] = useMutation(CREATE_USER, {
		onError: (err) => {
			alert(err.message)
		},
		onCompleted: (data) => {
			console.log(data)
			alert('Register successfully')
		}
	});

	const handleRegister = (event: React.FormEvent) => {
		event.preventDefault()
		createUser({
			variables: {
				input: {
					username,
					password,
					phone,
					email
				}
			}
		})
	}

	return (
		<section className={styles.container}>
			<Home />
			<div className={styles.regisContainer}>

				<form className={styles.regisContent} onSubmit={handleRegister}>
					<h2 className={styles.login_title}>REGISTER</h2>

					<div className={styles.usenameRegis}>
						<p>Use Name: </p>
						<input
							type="text"
							placeholder="Enter your username"
							onChange={(e) => setUsername(e.target.value)}
						/>
					</div>
					<div className={styles.passwordRegis}>
						<p>Password: </p>
						<input
							type="password"
							placeholder="Enter your password"
							onChange={(e) => setPassword(e.target.value)}
						/>

					</div>
					<div className={styles.phoneRegis}>
						<p>Phone: </p>
						<input
							type="text"
							placeholder="Enter your phone"
							onChange={(e) => setPhone(e.target.value)}
						/>
					</div>
					<div className={styles.emailRegis}>
						<p>Email: </p>
						<input
							type="email"
							placeholder="Enter your email"
							onChange={(e) => setEmail(e.target.value)}
						/>
					</div>
					<div className={styles.register_submit}>
						<button type='submit'>Register</button>
					</div>
				</form>
			</div>
		</section>

	)
}
export default RegisterPage;